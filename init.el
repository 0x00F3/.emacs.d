(add-to-list 'load-path "~/.emacs.d/load-path/")
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.njk\\'" . web-mode))

;; indentation
(setq-default indent-tabs-mode nil)
(setq-default tab-width 2) ; or any other preferred value
(setq tab-width 2)
(defvaralias 'c-basic-offset 'tab-width)

;; packages
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
  (add-to-list 'package-archives
               '("melpa-stable" . "https://stable.melpa.org/packages/") t)
  ;; run "M-x package-refresh-contents" to update package list every once
  ;; in a while. 
  )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (elm-mode ## markdown-mode scala-mode typescript-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
